@extends('layouts.app')

@section('content') 

<div class="text-center m-5"><h2>Les Différents Minerais</h2></div>

<div class="card-group">
  <div class="card">
    <img class="card-img-top" src="/img/Chomdû.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title text-center">Chomdû </h5>
      <p class="card-text">Le Chomdû est un minerai qui provoque des états dépressifs. Bizzarement, les personnes âgées ne sont plus affectées... .</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
  <div class="card" >
    <img class="card-img-top" src="/img/Klingon.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title text-center">Klingon</h5>
      <p class="card-text">Le Klingon, un peu relou, altère les combinaisons, traverse les matériaux et provoque des démangeaisons cutanées,  On a observé une résistance chez les geeks.</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="/img/Perl.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title text-center">Perl</h5>
      <p class="card-text"> Le Perl est le minerai à éviter à tout prix ! Hystérie, dépression, folie, voir mort subite sont les effets fréquemment observés..</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
</div>


@endsection