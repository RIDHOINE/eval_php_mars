<html>
    <head>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

     <title>Let's colonize Mars</title>
     
 
    </head>
    <body>
        @section('sidebar')
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
           <img class="navbar-brand" src="/img/planetMars_planet_2769.png" style=width:4%>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> </button>

           <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                 <li class="nav-item active">
                    <a class="nav-link font-weight-bold text-danger" href="home">HOME <span class="sr-only">(current)</span></a>
                 </li>
                 <li class="nav-item">
                    <a class="nav-link font-weight-bold text-danger" href="zone">ZONE</a>
                </li>
                <li class="nav-item"> 
                   <a class="nav-link font-weight-bold text-danger" href="minerais" >MINERAIS<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item"> 
                   <a class="nav-link font-weight-bold text-danger" href="suivi">SUIVI MARTIAN<span class="sr-only">(current)</span></a>
                </li>
             </div>
                  </li>
                      </ul>
                      
                          <div><button type="button" class="btn btn-danger">Me connecter</button></div>
                             </div>
                          <div><button type="button" class="btn btn-danger m-2">Créer un compte</button></div>
                             </div>
         </nav>
        @show

        <div class="container">
            @yield('content')
        </div>
        

        <footer>
            <div class="container">
                <p style="text-align:center;">© Let's colonize Mars !</p>
            </div>
        </footer>

    </div>
    </body>
</html>




 
 
 
