@extends('layouts.app')

@section('content') 

<div class="container">
  <div class="row">
    <div class="col border border-dark m-lg-5">
     <div class="m-3"><h3 class="text-center"> Cartographie de Mars </h3> </div>
     <div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23137.685803393888!2d1.5032239048652416!3d43.539649114004234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12ae95f2e0e4efad%3A0x2441ff13d6c0966e!2zTGFiw6hnZQ!5e0!3m2!1sfr!2sfr!4v1575974028057!5m2!1sfr!2sfr" width="500" height="400" frameborder="0" style="border:0;" allowfullscreen="" class="">></iframe> </div>
     <div><h4 class="text-center m-4">Signaler une zone</h4></div>

     
     <form action="/commentaire" method="POST" id="comme"> 
  <div class="form-row">
     @csrf
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Password</label>
      <input type="password" name="password" class="form-control" id="inputPassword4" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">Cordonnées GPS</label>
    <input type="text" name="gps" class="form-control" id="inputAddress" placeholder="47.6193757">
  </div>
  <div class="form-group">
    <label for="">Observations</label>
    <input type="text" name="observation" class="form-control" id="symptome" placeholder="Gaz, nuage roches sédimentaires, taille & variété des minerais">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
    </div>
    <div class="form-group">
      <label name="minerais" for="">Minerais</label>
      <input type="text" name="minerais" id="minerais">
    </div>
  </div>
  <div class="form-group">
    <div class="form-check">
    </div>
  </div>
  <button type="submit" class="btn btn-danger">Envoyer</button>
</form>
    </div>

    <div class="col m-lg-5">
        <div class="m-3"><h3 class="text-center"> Commentaires </h3> </div>
    </div>
  </div>

@endsection