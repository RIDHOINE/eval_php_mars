@extends('layouts.app')

@section('content') 
<div class="text-center m-5"><h2>La Communauté Vous Informes</h2></div>

<form action="/suivi" method="GET" id="comme" class="">
  <div class="form-row">
  @csrf 
      <div class="form-group col-md-6">
  <div class="form-group">
    <label for="">Observations</label>
    <input type="text" class="form-control" id="symptome" placeholder="Gaz, nuage roches sédimentaires, taille & variété des minerais">
  </div>
  <button type="submit" class="btn btn-danger">Envoyer</button>
</form>
   

<div class="d-flex bd-highlight">
  <div class="p-2 w-100 bd-highlight" id="message">Flex item</div>
  <div class="p-2 flex-shrink-1 bd-highlight" id="profil">Flex item</div>
</div>  


@endsection