<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/zone', 'ZoneControlller@index')->name('zone');
Route::get('/minerais','MineraisController@index')->name('minerais');
Route::get('/suivi', 'SuiviController@index')->name('suivi');
Route::get('/profil','ProfilController@index')->name('profil');

Route::post('/commentaire','HomeController@comme')->name('commentaire');
Route::GET('/suivi', 'SuiviController@comme')->name('commentaire');



