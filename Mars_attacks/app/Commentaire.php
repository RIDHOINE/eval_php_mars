<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    //
    public function user()
    {
        return $this->belongsToMany('App\User');

    }
    protected $primaryKey = 'commentaire_id';
}
