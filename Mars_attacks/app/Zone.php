<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    //
    public function minerai()
    {
        return $this->belongsToMany('App\Zone');

    }
    protected $primaryKey = 'zone_id';
}
