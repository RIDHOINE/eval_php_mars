<?php

namespace App\Http\Controllers;
use App\Commentaire;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;


class HomeController extends Controller
{
    //
    public function index()
    {
        return view('home');
    }
    public function comme(Request $request, Redirector $redirect){
        $commentaire = new Commentaire;
        $commentaire->email = $request->input('email');
        $commentaire->password = $request->input('password');
        $commentaire->gps = $request ->input('gps');
        $commentaire->observation = $request ->input('observation');
        $commentaire->minerais = $request ->input('minerais');

        $commentaire->save();
        
        return redirect('home');
    }
}
