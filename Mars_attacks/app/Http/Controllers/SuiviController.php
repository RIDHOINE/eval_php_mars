<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Commentaire;

class SuiviController extends Controller
{
    //
    public function index()
    {
        return view('suivi');
    }
    public function comme(Request $request, Redirector $redirect){
        $commentaire = new Commentaire;
        $commentaire->observation = $request ->input('observation');

        return redirect ('suivi');
    }
}
